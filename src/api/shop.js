import axios from '@/libs/api.request'
import querystring from 'querystring'
import http from './public'

// 获取商家列表
export const getShopList = (queryParams) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/shop/admin/list',
    method: 'post',
    data: queryParams
  })
}

//增加
export const add = (shopForm) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/shop/add',
    method: 'post',
    data: shopForm
  })
}

//修改
export const edit = (shopForm) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/shop/update',
    method: 'post',
    data: shopForm
  })
}

// 删除选中
export const removeAll = (shopList) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/shop/deleteAll',
    method: 'post',
    data: shopList
  })
}
//导出表格
export const exportExcel = (shopList) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/shop/export',
    method: 'post',
    data: shopList
  })
}

// 获取商家详细信息
export const detail = (shopId) => {
  return axios.request({
    url: '/api/shop/detail?shopId='+shopId,
    method: 'get',
  });
}
