import axios from '@/libs/api.request'
import querystring from 'querystring'

export const userSubmit = (user) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/user/handle',
    method: 'post',
    data: user
  })
}

export const logout = (token) => {
  return axios.request({
    url: 'logout',
    method: 'post'
  })
}


export const getInfo = (id) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/user/info',
    method: 'post',
    data: id
  })
}

// 获取用户列表
export const getUserList = (params) => {
  let query = querystring.stringify(params)
  return axios.request({
    url: '/api/user/list?' + query,
    method: 'get'
  })
}

// 删除用户
export const remove = (id) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/user/delete',
    method: 'post',
    data: id
  })
}

// 批量删除
export const removeAll = (userList) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/user/deleteAll',
    method: 'post',
    data: userList
  })
}

// 新增用户
export const add = (user) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/user/add',
    method: 'post',
    data: user
  })
}

// 修改用户
export const edit = (user) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/user/edit',
    method: 'post',
    data: user
  })
}

export const findAll = () => {
  return axios.request({
    url: '/api/user/all',
    method: 'get'
  })
}
