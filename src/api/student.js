import axios from '@/libs/api.request'
import querystring from 'querystring'
import http from './public'

// 查询学生列表
export const student_list = (page, size, params) => {
  let query = querystring.stringify(params)
  return axios.request({
    url: '/api/student/list/' + page + '/' + size + '/?' + query,
    params: {
      params
    },
    method: 'get'
  })
}
// 查询班级id，名字
export const class_list = () => {
  return axios.request({
    url: '/api/class/get/list',
    method: 'get'
  })
}

// 添加学生
export const addStudent = (addForm) => {
  return http.requestPost('/api/student/add', addForm)
}
// 删除学生
export const delStudent = (delList) => {
  var del = JSON.stringify(delList)
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/student/delete',
    method: 'post',
    data: del
  })
}

//根据id查询学生信息
export const findStudentById =(id) => {
  return http.requestGet('/api/student/get/'+id)
}
//根据id查询学生学籍信息
export const findLearnById =(id) => {
  return http.requestGet('/api/class/learn/get/'+id)
}
//修改学生信息
export const editStudent= (form)  => {
  return http.requestPost('/api/student/edit', form);
}
