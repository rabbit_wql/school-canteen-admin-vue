import axios from '@/libs/api.request'

// 获取订单列表
export const getOrderList = (queryParams) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/order/admin/list',
    method: 'post',
    data: queryParams
  })
}
// 获取订单4状态数量
export const getOrderNum = () => {
  return axios.request({
    url: '/api/order/admin/num',
    method: 'get'
  })
}

// 删除选中
export const remove = (orderId) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/order/delete',
    method: 'post',
    data: orderId
  })
}


// 获取商家详细信息
export const detail = (orderId) => {
  return axios.request({
    url: '/api/order/detail/'+orderId,
    method: 'get',
  });
}

export const findAll = () => {
  return axios.request({
      url: '/api/order/all',
      method: 'get'
  })
}

