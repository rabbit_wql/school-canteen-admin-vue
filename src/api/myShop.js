import axios from '@/libs/api.request'

// 获取商店信息
export const getShopInfo = (id) => {
  return axios.request({
    url: '/api/shop/info/' + id,
    method: 'get'
  })
}

// 轮询
export const check = (id) => {
  return axios.request({
    url: '/api/order/check/' + id,
    method: 'get'
  })
}

//修改基本信息
export const editBase = (form) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/shop/editBase',
    method: 'post',
    data: form
  })
}

// 修改图片
export const updateImg = (shop) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/shop/editImg',
    method: 'post',
    data: shop
  })
}

// 订单列表
export const orderList = (queryParams) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/order/admin/list',
    method: 'post',
    data: queryParams
  })
}

// 订单各数
export const initNum = (id) => {
  return axios.request({
    url: '/api/order/admin/num?id='+id,
    method: 'get'
  })
}


//获取菜单列表
export const getFoodList = (queryParams) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/food/admin/list',
    method: 'post',
    data: queryParams
  })
}

//获取评论
export const getComment = (foodId) => {
  return axios.request({
    url: '/api/food/comment/'+foodId,
    method: 'get'
  })
}

// 添加或修改商品
export const addOrUpdateFood = (food) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/food/addOrUpdateFood',
    method: 'post',
    data: food
  })
}

// 删除所选商品
export const removeAll = (foodList) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/food/removeAll',
    method: 'post',
    data: foodList
  })
}

// 删除回复
export const removeComment = (id) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/food/reply/remove',
    method: 'post',
    data: id
  })
}

// 回复
export const addReply = (id, reply) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/food/reply/add',
    method: 'post',
    data: {
      id: id,
      reply: reply
    }
  })
}