import axios from '@/libs/api.request'

// 获取日志列表
export const getLogList = (queryParams) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/log/admin/list',
    method: 'post',
    data: queryParams
  })
}

// 获取商店日志列表
export const getShopLogList = (queryParams) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/log/shop/list',
    method: 'post',
    data: queryParams
  })
}
// 删除选中
export const remove = (id) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/log/delete',
    method: 'post',
    data: id
  })
}
// 删除选中
export const removeAll = (shopList) => {
  return axios.request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: '/api/log/deleteAll',
    method: 'post',
    data: shopList
  })
}

export const findAll = () => {
  return axios.request({
    url: '/api/log/all',
    method: 'get'
  })
}

export const findShopLogAll = (id) => {
  return axios.request({
    url: '/api/log/shop/all/' + id,
    method: 'get'
  })
}
